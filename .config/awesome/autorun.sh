#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run xrdb -merge ~/.Xresources
run setxkbmap -option ctrl:nocaps
run urxvtd -q -f -o
